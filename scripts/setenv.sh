#!/bin/sh

export IMAGE_NAME=animalmutha76/ssak
export IMAGE_VARIATION=slim
export ARCH=amd64
export DEB_URL=https://dl.google.com/linux/direct/google-chrome-stable_current_${ARCH}.deb
export TMP=tmp
export CHROME_DEB=${TMP}/chrome.deb
export AWSCLI_ZIP=${TMP}/awscliv2.zip
export TERRAFORM_ZIP=${TMP}/terraform.zip
export TERRAGRUNT_EXE=${TMP}/terragrunt
export AWSCLI_URL=https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip
export TERRAFORM_URL=https://releases.hashicorp.com/terraform/1.8.2/terraform_1.8.2_linux_amd64.zip
export TERRAGRUNT_URL=https://github.com/gruntwork-io/terragrunt/releases/download/v0.57.12/terragrunt_linux_amd64
