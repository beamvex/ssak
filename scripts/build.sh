#!/bin/sh

. ./scripts/setenv.sh

mkdir ${TMP}

if [ ! -f ${CHROME_DEB} ]; then
    wget ${DEB_URL} -O ${CHROME_DEB} 

fi

if [ ! -f ${AWSCLI_ZIP} ]; then

    wget "${AWSCLI_URL}" -O "${AWSCLI_ZIP}"

fi

if [ ! -f ${TERRAFORM_ZIP} ]; then

    wget "${TERRAFORM_URL}" -O "${TERRAFORM_ZIP}"

fi


if [ ! -f ${TERRAGRUNT_EXE} ]; then

    wget "${TERRAGRUNT_URL}" -O "${TERRAGRUNT_EXE}"

fi

IMAGE_VARIATION=${1}

docker build -t ${IMAGE_NAME}:${IMAGE_VARIATION} -f Dockerfile.${1} .  