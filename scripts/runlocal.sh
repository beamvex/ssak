#!/bin/sh

. ./scripts/setenv.sh

ENTRYPOINT="/bin/bash"

docker run -it --rm --entrypoint "${ENTRYPOINT}" --name test \
    ${IMAGE_NAME}:${IMAGE_VARIATION} \
    $1